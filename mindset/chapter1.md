---

title: chapter 1 summary.


---
[Two camps]
Those who start the smartest do not end up the smartest.

There are those who agree with this statement, with enough education and practice ANY of us can become the smartest.

And there are those who don't believe, they believe their intelligence is a fixed trait "carved in stone".


[Carol Dweck intro]
In her book , Mindset, researcher Carol Dweck categorises these two mindsets into FIXED mindset and GROWTH mindset.

She came across these two mindsets when she observed her students coping with failures.
Fixed: You were smart or not smart. Failing meant you were not
Growth: Don't think about failure, but learning.


[Alien]
Those who believe that you can develop your intelligence may look upon people with a FIXED mindset as pessimistic or
suffering from low self-esteem.

Those who have a fixed mindset may look upon those with a GROWTH mindset as aliens. Who see failure in tests as an
opportunity to learn rather than a DUMB stamp on their forehead. Naive maybe.


[Coping]
A major difference between these two groups was how they coped with failure.
FIXED: They don't put any time or effort at all. "If Rome wasn't built in a day, then maybe it wasn't meant to be."
GROWTH: They believed every failure was an opportunity to learn.


[Learning]
People with GROWTH mindset actually enjoyed when they failed as it gave them an opportunity to develop their skills.
People with FIXED mindset detest failure so much that they proceed to avoid situations where they fail, and seek out
situations where they know they will succeed.


[Proving themselves]
It is a difficult mindset to be in as it results in them seeing every situation as an evaluation. Some outcomes are magnified
and some are explained away. They are super sensitive in these situations and hate being wrong or making mistakes.

