---

layout: Giving up

---


[Giving up before you start]
You can fall at the first hurdle a lot. y which I mean no teven trying,. Not even attempting it.
There are many reasons you can coe up with. Usually, they are all valid ones.  Setting yourself a goal and not attempting
anything at all in regard to it is one of the most common failures.


[Giving up after you start]
Once you start something new, it can be quite intimidating. You need to adapt fast. You are entering a new world where you don't belong but you
thinki it's right for you. You don't have any of the equipment or knowledge of how it works. You can't decide what is the
best approach for it.

You can fall after the first hurdle when there is no clear guidance for you. You are aimless. You don't have the right
equiopment. It can be demoralising when you don't have the support that you need until you can support yourself.


[Giving up before the end]
How long before you know your finished? How long do you keep to a rigourous schedule before you get the desired results?
Question, will you ever meet your desired results?