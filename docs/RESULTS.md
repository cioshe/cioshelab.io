---

layout: Results.

---

Knowing how to measure progress. How to see results.

When starting on a change, the only thing we are interested in is results. We want to look different, feel healthier.
We won't be happy until we achieve that goal.

This is a noble cause but without the right mindset it can be difficult to achieve.

In order to see results CICO style. We must up our exercise and reduce our calories until we are burning more energy than
we consume.  Simple.

So simple actions are to cut back on food and up the amount of times we go to the gym. The problem is that these are habits
that require a sizeable amount of willpower. And willpower is a muscle that we don't exercise that readily.

So crash diets and boot camps are a good way to see results. I totally advise in doing those types of those shocks to your
comfort zone so that you are forced to adapt and learn new things about yourself. Experimenting with different lifestyles
is the best way to learn what works for you.

However, they do not last in the long-term. Simple swaps of a certain food are shown to require little effort but if they
are stuck too then they are proven to reduce weight over the long term. The only problem is that they take a long time
to see results. This is a problem for most people.