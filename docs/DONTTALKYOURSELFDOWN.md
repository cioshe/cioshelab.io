---

layout:  Don't talk yourself down

---

You've talked yourself out of losing weight too many times.

You've blamed somebody or made excuses before and you've forgotten how many times you have to be honest. Which is a pity,
because you do want to make a change. It's just you've got this life that has too much momentum to fight against.

The effort you put in seems to just not even make a slight impact on your goals.

How many times have you decided not to go for it.

How many times did you convince yourself it wasnt' the right time, or you'll do it some other time?

I've done this before. When trying to start a time restricted eating plan. What was prescribed was fasting for 16 hours.
When I failed in my attempts I concluded that it wasn't for  me. Not knowing that 14 hours fasting or 12 horus fasting
still provided benefits that I was looking for. I had a fixed mindset towards the problem.

