---
layout:  isdone
title: "reset "
---


Get back on the horse. Don't wallow in your self defeat.

it's tough. Fighting against your desires. Flowing against the stream.

You don't know when life will throw the next curveball at you. Life is unpredictable. You cannot control it. You can only be prepared to accept what is or isn't in your control

What I find though is it gets easier. Going from zero is hard to stomach because there is a lot of work to do. But it gets easier.

The more you practice, the more it becomes habit and you don't have to think about it anymore.

Autopilot is your friend. Humans have evolved to block out the unnecessary because if we paid attention to all the things then
we would die. We are tuned to pick up certain things we are not tuned to succedd in every dimension of our life. That is
just way too stressful.