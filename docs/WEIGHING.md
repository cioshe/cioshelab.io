---

title: Weighing scales

---

From where does the fixed mindset grow about food?

Our weight it something which we evaluate regularly on a regular basis. It provides us with valid data points on our progress

Also it reaffirms our identity when we receive comments on our weight. You look like you lost weight is something that people
would love to hear. You've put on weight is something that could devastate you.

And to be honest. No one is interested in any feeback on how to change their weight. You should go to the gym three times a week,
you should eat 30g of protein every morning, you should do intermittent fasting.

None of this sinks in to someone with a fixed mindset.


