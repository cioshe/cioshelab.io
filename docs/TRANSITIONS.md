---

layout:  Transitions

---


The decision to eat for the better is usually depressing. It is a commitment to a life that you want but you also don't
want. It involves looking at yourself in the mirror and not letting any excuses derail you. YOu need to be strong,
there are going to be some weak points where you will be emotional.

(It reminds me of a time when Conor McGregor was being interviewed for his title fight. Cutting weight is emotional as he
put it)

It only works when you don't give yourself a way out. When you put yourself all in it betters you in many ways. You can be
proud of coming out the other end. If it is too difficult though we can convince ourselves that we didn't try in the first
place so we never really did fail. You can lose a race you didn't start.

A way of protecting ourselves against the fear of failure. <Fixed Mindset> But what you should really be afraid of is not trying in the
first place. There are plenty of reasons no too but there are even more reasons why you should.

This is the mental aspect of switching from eating normally to eating healthier. It is full of dread, anticipation, dull
and boring food. Like treating yourself as a child.

The other aspect of switching is how prepared you are. I mean, in short, is preparing your current self against your future
self. You can anticipate your excuses.

"I can't do the diet because I don't have enough celery"
<Then go buy a months worth of celery>

The reason for this is not because you are going to eat all of it. You may only eat some. The point is to defuse that way
of thinking. To block the fire exit and keep you in. To no let yourself mentally exit the building. Once this happens,
you are done. All the mental anguish and loathing that you went through just to get you to start, and you fell at the first
hurdle.

This failure is not a failure of will, it is a failure or preparation.



The decision to stop eating for the better is usually depressing. You are giving up a life of health and longevity for
a life of short term satisfaction. You are selling your health short for a few quick "morale boosts" <Reminds me of Carl
where biscuits were the only morale booster that he has>

If you only lasted a bit longer, if you only held out a little more. But it's probably gotten too much at this stage.
The bbqs are getting harder and harder to attend. It's getting tougher to resisst your friends attempts at subterfuge.
(I like that word).

When the mental toll of keeping up the phase is beginning to cost you something in your personal life. It is time to reign
yourself in. It may be time to allow yourself out of your commitment to let yourself change your mind.


But why should you allow yourself this kindness?


It is time to realise that this period of healthy eating was not a failure. While you don't have the will to keep it going
you can reflect on what the high times and low times were. Maybe mention the peak/end rule.

If you can reflect on your time past and just choose one thing that you can do for life. ONE THING. Then you have successfully improved your well
being and the whole experiment was worth it.

The question now is, can you do another experiment?