---

title:  What to eat

---


I remember leaving work. It was a dark evening, raining and grey overcast clouds were about. I sat into my car and as I
was leaving I remember it was my turn to grocery shop. I rang my partner and asked her what she wanted to eat.

After a short silence she responded with "I don't know, what do you think". It's not an easy thing to do. To switch on and
just know what you want to commit to eating. THe mind is not ready, it needs to be readied.

I threw out a few options and they were shot down, things started to get snarky.

It was going no where, neither one of us were ready to take responsibility for the meal choices, so I went with the safest
choice available to us. "I'll just get the usual."