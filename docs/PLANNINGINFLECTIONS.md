---

title:  planning inflection points

---

I recognise that I have my triggers.

I say to myself that I will not go to the vending machine but 30 minutes later I am pulling out another bag of
maltesers and berating myself for being weak.

I know that when I go for a tea break next to the vending machine my will power will fade eventually.

If I have any spare coins on me there is a 50/50 chance that they are going in to the vending machine.

I can tell myself as much as I want that "I'm not going to do this again" but it is a hopeless promise. I don't really
believe that I can do it.

These points as Charles Duhigg descritbes them are inflection points. He has described them for patient who are recovering
from accidents and require major recovery. For unhealthy eating, the vending maching is my source of pain.

Mental anguish over what I can't get over.


As the patients who prepare for moments of pain, we can come up with a plan to tackle these moments.

We can practice a couple of strategies.
Avoidance
Postponement
Replacement
Forgiveness

