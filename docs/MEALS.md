---

layout:  Get your meals right!

---
[Struggling - it applies to everyone somewhere]

Struggling is natural in a difficult situation.

Everybody struggles at something, at some stage in their life. School, swimming, debt, ... health eating.

Carol Dweck did a study back in 1970 or something when she observed how from a broad range of students they reacted to
struggling in two different ways. Some crumbled under the pressure, others rose to the challenge.

What is the difference between these two set of students. Growth mindset and Fixed mindset.


When it comes to dieting those who have a fixed mindset are quite obvious. You are what you eat! I can't do this!
I didn't even try! I'm big boned! It's in my genetics!

Fixed mindset is prelevant acrooss all kinds of preple when it comes to losing weight.


A fixed mindset makes it inevitable for failuure.

Not only is this inevitablility frustrating but it is TIRING. It is all consuming of your energy. It is a constant in the back of your
mind.  <Mention flow here maybe>

In those moments we need something to grab onto for support. But what?

Something to help out, take some of the burden. So we can just let go.

It will give us room to breathe, collect our thoughts that we were to anxious or worried to deal with.


[Transition to dieting]
When it comes to dieting, everybody thinks of giving up at some stage. Everybody feels that there are times when it is
too much. Too hard to resist.

What happens when they just let go. They derail, fall off the wagon and it is HARD to get back on the horse.
So what can they grab onto to get back.

