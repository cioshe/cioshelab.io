Last week, for one day I was vegan, by accident. I didn't intend for it to be the Tuesday that I would focus on being vegan, it was afterwards
that this realisation came to me. By not focusing on the outcome, and focusing on habits and routines instead, the results have started to show.

An accidental vegan is not a vegan. It is only some of the time, like a flexitarian.

The difference between an accidental vegan and a flexitarian is that an accidental vegan aspires to be vegan, although admits it is too
difficult to achieve at this point in their lives. A flexitarians ambition is to just eat less meat, but does not aspire to go further.

The vegan lifestyle is generally advocated by people with a strong moral compass, that will guide them in their daily lives quite easily to eat
the right food. There are those of course who lack a moral compass to guide them through everyday temptations to eat meat, although they would
subscribe to the vegan lifestyle if it was more accessible to them.

For these people, it is not the fault of theirs that they cannot commit to the lifestyle. To turn yourself from being a person who grew up with
meat every day, to a person who eats none overnight is a monumental challenge. The comfort food that you can no longer eat, the recipes you can
no longer cook, it leaves a vacuum in your life that you are not yet ready to fill.

Going cold turkey is a method that will work but the shock can be too great to your system, and, may result in you giving up on your aspiration
completely and resigning to a life less fulfilling.

 An accidental vegan subscribes to the following instead.
   1. I cannot be vegan now, but I will be in the future.
   2. I cannot eat all vegan food now, but I will discover more that I like.
   3. I cannot abstain from all meat now, but I will learn to live without.
   4. I will do as much as I am able to do.

