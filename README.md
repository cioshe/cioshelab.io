![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

GitBook website for Deliberate Meals

---

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Meals](docs/MEALS.md)
- [An Introduction](#an-introduction)
- [Troubleshooting](#troubleshooting)
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## An introduction

Some interesting quesions I needed answers to before I considered writing a blog.

From Ryan Holiday,
"If you want to be a writer then must have something to say"

What I want to say is secondary but I want it to make an introduction. I want to introduce you to a new way of thinking,
albeit through a blog, book or person. This introduction, I'm hopeful, will bring about change, if not, then I have not
said what I wanted to say


From Annie lamott,
"Writing is about speaking the truth."

The question is can I do this. A lot of what comes out of writing is self-indulgent. Can I just speak the truth?


From "Ramit Sethi",
"How can I add value?"

Whatever I write needs to add value to the person reading it.


Also, from book guy, "What of it?"
Why is it important to me? Why am I telling the world about this.


From Dan Carlin,
"I don't need you to believe what I say, but I am a fan. I can tell you what this person said or that person said"

So what? I wrote this paragraph, so what?


## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
