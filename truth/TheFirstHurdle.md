---

title: The First Hurdle.

---

[Prelude to mindset blog posts would be aspirations/ambitions/goals/purpose]
The person reading this must have aspirations or goals already they have put their mind to. They are now considering
a plan of action

[Advice]

It's tough in the beginning. The best advice I can give is to find a way to stick with it.

Taking on a new plan and trying out the first step in a given goal is the preparatino phase. Now you are beginning
the action phase. You have to stop thinking about what you're going to do  and you need to do it.

To lose weight and see results you are going to go with a given meal plan and exercise plan. Proven that you will lose
weight. You don't try to think about it too much because you will probably talk yourself out of it.

So you start. Congratulations if you got this far. This is getting over the first hurdle.

But be warned. Once you start you will come up against tough times. Emotional times where you won't be able to eat the
desert or drink the beer you are used. At a time of the week that you are used to it!!

It is easy to give up. Don't. So find a way to stick with it.

How?

Break down your


[Introduction]

A problem I had before was when I wanted to try Intermittent Fasting for the first time. I took on their rules of 16 hours on, 8 hours off.
I tried it, found it too difficult and decided it wasn't for me.

Months later, I came across a post about Time Restricted eating. A different fad but I listened. Most people eat on a 15 hour window.
Studies have shown that eating in a 12 hour window is better. Eating in  10,9,8 window is better again. THe benefits of
Time restricted eating are this. The more you work at it the better you will become.

The difference in both eating behaviours differs in a distinct way. Intermittent Fasting rules as I have layed them out
breach your fixed mindset.

The Time restricted eating instruction instill a growth mindset.

Don't fall into the same trap as I did.

I approached the Intermittent Fasting not knowing what to expect. I correctly believed the hype. I think I tried it out
because it was a fad. I wasn't even sure as to why I was doing it. I just decided it was worth a shot. What had I got to
lost. Fasting was something that I had come across again and agin and I was ready to give it a try.

In hindsight, it was a wasted experiment. Not only because it turned me off fasting as a weight restriction approach but it
had an overall negative impact on my eating behaviour.

Time restricted eating on the other hand I still practice.




In the beginning you've gone through  the process of deciding this ais a goal worth pursuing, but you don't know where to start.
Beginning yorus research on the internet can lead you down a rabbit hole of advice, alot of which would be contradicting.

You can get overwhelmed.

Or it can happen other ways, you make a plan to go to the gym three days a week, yet you are like a student falling asleep
thrying to do their homework.

Your willpower you once had fades, and you come to a moment of realisation that it is no longer a notion living in your head
but a real plan. A time a place soon. It can start to become to daunting and you might get a bit flaky.



Starting from zero is hard to stomach because there is a lot of work to do. There is a long road ahead.
It is depressing to look at the uphill battle in front of you and to take the first step. So you consider backing out and
not doing ANYTHING.

Don't. Give. Up. This is the first hurdle.

||| Note to self. Given that I am up to convincing people. I would like to take a Plato style approach here.
||| Come up with hypothetical counterarguments to this and then to disprove them. Each counterargument should be accepted
||| for validity. Demonstrated to be invalid by social proof. (no time, show the life of someone who is too busy)

So what if you do you back out. It happens to the best of us, right?.

It is ok to back out of something in the moment, with a decision to come back to it later. What is not ok is that you
back out of the plan completely! Why should you allow yourself to give up on your ambition????

Let's look at ourselves honestly. "It happens to the best of us" is just another way of allowing us to accept not trying
because the task is overwhelming. You have made a decision right there to not try. You can't lose a race you didn't start.

The first lesson to overcome this mindset. A fixed mindset that your abilities are limited. Sure, you can accept that you
don't have the ability to take the first step now. But you must accept that with time and effort you will be able to.

Our mindset is the first obstacle we must learn to overcome. (The obstacle is the way}

Not what are the right foods to eat. Not the gym membership. THe first lesson to learn is the your <mindset|link> can be your
worst enemy. It will bring you down.

You can change your mindset by just being aware of it.

[Fear of Failure, ego]

[Not knowing how to start]
Advice on eating healthy is not easy to absorb. In general, it appears as an empty list of unconnected pointers.
Where you don't know what leads to another.






