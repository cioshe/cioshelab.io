---

title: Be wary of health advice

---


Be wary of advice like this

Eat in 12 hours a day

Try one new vegetable a week

Eat 30g protein in the morning

Eat no carbs


This advice is not suspect because it is wrong but because of the message it delivers.

You do it, you're healthy. You don't, you're a failure.

This message is a fixed mindset message and you should be wary of them

[Eat 12 hours a day]

<Reason for this...>

<How can it be misinterpreted...>

<How can a new growth mindset message be learned from it>

[Try one new vegetable a week]
[Eat 30g protein within 30 minutes of waking]
[No carbs]
